import React from 'react';

const TodoItem = props => (<div>
  <input type="checkbox" checked={props.complete} />
  {' '}<span style={{
    textDecoration: props.complete ? 'line-through' : 'none',
  }}>
    {props.label}
  </span>
</div>);

TodoItem.propTypes = {
  complete: React.PropTypes.bool.isRequired,
  label: React.PropTypes.string.isRequired,
};

export default TodoItem;
