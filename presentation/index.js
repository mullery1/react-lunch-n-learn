// Import React
import React from 'react';

// Import Spectacle Core tags
import {
  Deck,
  Heading,
  Slide,
  Spectacle,
  Text,
  List,
  ListItem,
  Appear,
  CodePane,
} from 'spectacle';

// Import image preloader util
import preloader from 'spectacle/lib/utils/preloader';

// Import theme
import createTheme from '../theme';

// Require CSS
require('normalize.css');
require('spectacle/lib/themes/default/index.css');


const images = {
  spaghetti: require('../assets/spaghetti.jpg'),
};

import reactSlides from './React';
import reduxSlides from './Redux';
import reactRouterSlides from './ReactRouter';

import notes from './notesify';

preloader(images);

const theme = createTheme({});

export default class Presentation extends React.Component {
  render() {
    return (
      <Spectacle theme={theme}>
        <Deck transition={['fade']} transitionDuration={300} progress="bar" bgColor="quartenary" children={[
          (<Slide bgColor="primary" id="title">
            <Heading size={1} fit caps lineHeight={1} textColor="black">
              Web apps with
            </Heading>
            <Heading size={1} fit caps>
              React.js
            </Heading>
            <Heading size={1} fit caps textColor="black">
              and Redux
            </Heading>
            <Text textSize="1.5em" margin="20px 0px 0px" bold>Ryan Muller</Text>
            <Text textSize="1.3em" margin="20px 0px 0px">March 16, 2016</Text>
          </Slide>),
          (<Slide bgColor="black" id="overview">
            <List>
              <Heading>Overview</Heading>
              <Appear><ListItem children={"React"}/></Appear>
              <Appear><ListItem children={"Redux"}/></Appear>
              <Appear><ListItem children={<code>react-router</code>}/></Appear>
              <Appear><ListItem children={"CSS Modules"}/></Appear>
              <Appear><ListItem children={"Browserify"}/></Appear>
            </List>
          </Slide>),
          ...reactSlides(),
          ...reduxSlides(),
          ...reactRouterSlides(),
          (<Slide id="css-modules" bgColor="tertiary" notes={notes(`
            `)}>
            <Heading size={1} caps textColor="black">
              CSS Modules
            </Heading>
          </Slide>),
          (<Slide id="buttons-css-1" bgColor="tertiary">
            <Heading textColor="black" size={5}>buttons.css</Heading>
            <CodePane lang="jsx" source={require('raw!../assets/buttons-1.css.sample')} textSize="0.7em" />
          </Slide>),
          (<Slide id="buttons-css-2" bgColor="tertiary">
            <Heading textColor="black" size={5}>buttons.css</Heading>
            <CodePane lang="jsx" source={require('raw!../assets/buttons-2.css.sample')} textSize="0.7em" />
          </Slide>),
          (<Slide id="buttons-css-3" bgColor="tertiary">
            <Heading textColor="black" size={5}>buttons.css</Heading>
            <CodePane lang="jsx" source={require('raw!../assets/buttons-3.css.sample')} textSize="0.5em" />
          </Slide>),
          (<Slide id="css-modules-sample" bgColor="tertiary">
            <CodePane lang="jsx" source={require('raw!../assets/css-modules.js.sample')} textSize="0.7em" />
          </Slide>),
          (<Slide id="browserify" bgColor="tertiary" notes={notes(`
            `)}>
            <Heading size={1} caps textColor="black">
              Browserify
            </Heading>
          </Slide>),
          (<Slide id="end" bgColor="black" notes={notes(`
            `)}>
            <Heading size={1} caps textColor="white">
              end
            </Heading>
          </Slide>),
        ]} />
      </Spectacle>
    );
  }
}
