// Import React
import React from 'react';

import notes from './notesify';

// Import Spectacle Core tags
import {
  Slide,
  Heading,
  CodePane,
  Layout,
  Fit,
  Fill,
} from 'spectacle';

export default () => [
  (<Slide id="what-is-redux" bgColor="tertiary" notes={notes(`
    What is Redux? Now we\'re getting into framework territory.
    `)}>
    <Heading size={1} fit textColor="black">
      What is Redux?
    </Heading>
  </Slide>),
  (<Slide id="redux-is">
    <Heading fit>
      Redux models your app with
    </Heading>
    <Heading fit textColor="black">
      a single data Store
    </Heading>
    <Heading fit>
      which is updated by atomic actions
    </Heading>
    <Heading fit>
      and whose data is exposed as immutable.
    </Heading>
  </Slide>),
  (<Slide id="redux-diagram" bgColor="white" bgImage={require('../assets/redux.png')}/>),
  (<Slide id="redux-actions" bgColor="tertiary">
    <Heading fit caps textColor="primary">
      Actions:
    </Heading>
    <Heading fit textColor="black">
      &ldquo;something happened in my app&rdquo;
    </Heading>
  </Slide>),
  (<Slide bgColor="tertiary">
    <CodePane lang="jsx" source={require('raw!../assets/sampleAction.js.sample')} textSize="0.9em" />
  </Slide>),
  (<Slide id="redux-reducers" bgColor="tertiary">
    <Heading fit caps textColor="primary">
      Reducers:
    </Heading>
    <Heading fit textColor="black">
      &ldquo;how do I feel about that&rdquo;
    </Heading>
  </Slide>),
  (<Slide bgColor="tertiary" id="reducer-sample">
  <Layout>
    <Fit>
      <CodePane lang="jsx" source={require('raw!../assets/sampleReducer.js.sample')} />
    </Fit>
    <Fit>
      <CodePane lang="jsx" source={require('raw!../assets/sampleAction.js.sample')} />
    </Fit>
    <Fill>
      <CodePane lang="jsx" source={require('raw!../assets/sampleReducer-after.js.sample')} />
    </Fill>
  </Layout>
  </Slide>),
  (<Slide id="redux-store" bgColor="tertiary">
    <Heading fit caps textColor="primary">
      the Store:
    </Heading>
    <Heading fit textColor="black">
      &ldquo;who needs to know about this&rdquo;
    </Heading>
  </Slide>),
];
