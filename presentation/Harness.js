/* Allow `eval` because we know what we're doing. Hopefully. */
/* eslint no-new-func: 0 */
/* global babel */
import React, {Component} from 'react';

import Codemirror from 'react-codemirror';
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/mode/jsx/jsx';

export default class Harness extends Component {
    constructor(props) {
      super(props);
      this.state = {
        code: this.props.code || '<div className="well">\n  <h1>Hello, world</h1>\n</div>',
        component: null,
        error: null,
      };
    }
    renderError() {
      const {error} = this.state;
      if (!error) return [];
      return (<div>
          {error.loc ? <span>Error line {error.loc.line} column {error.loc.column}</span> : <span>Error</span>}
          <pre>{error.message.split('\n')[0]}</pre>
      </div>);
    }
    render() {
      const {
        cmOptions = {lineNumbers: true},
      } = this.props;
      const component = this.state.component || <span>No component returned</span>;
      return (<div className="Harness">
          <div className="Harness__left">
              {component}
          </div>
          <div className="Harness__right">
              <Codemirror value={this.state.code} options={{...cmOptions, mode: 'jsx'}} onChange={this.updateCode.bind(this)}/>
              <div className={`Harness__error${this.state.error ? ' active' : ''}`}>
                  {this.renderError()}
              </div>
          </div>
      </div>);
    }
    updateCode(newCode) {
      this.setState({
        code: newCode,
      });
      this.compile();
    }
    compile() {
      try {
        const compiled = babel.transform(`d(${this.state.code}\n);`, {
          filename: 'user input',
          comments: false,
        });

        let component = null;
        const display = (c) => component = c;

        const {expose = {}} = this.props;
        expose.React = React;
        expose.d = display;
        const func = new Function(...Object.keys(expose), compiled.code);
        func(...Object.keys(expose).map(k => expose[k]));
        this.setState({component, error: null});
      } catch (error) {
        console.error({ error }); // eslint-disable-line
        this.setState({error});
      }
    }
}

Harness.propTypes = {
  cmOptions: React.PropTypes.object,
  code: React.PropTypes.string,
  expose: React.PropTypes.object,
};
