import kramed from 'kramed';

export default function notes(n) {
  return kramed(n.replace(/\n\s{4}/g, '\n'));
}
