// Import React
import React from 'react';

import notes from './notesify';

// Import Spectacle Core tags
import {
  Slide,
  Heading,
  CodePane,
} from 'spectacle';

export default () => [
  (<Slide id="react-router" bgColor="tertiary" notes={notes(`
    `)}>
    <Heading size={1} fit textColor="black">
      <code>react-router</code>
    </Heading>
  </Slide>),
  (<Slide id="react-router-sample" bgColor="secondary">
    <CodePane lang="jsx" source={require('raw!../assets/react-router.js.sample')} textSize="0.7em" />
  </Slide>),
];
