// Import React
import React from 'react';

import notes from './notesify';
import Harness from './Harness';
import TodoItem from './TodoItem';

// Import Spectacle Core tags
import {
  Slide,
  Heading,
  CodePane,
} from 'spectacle';

export default () => [
  (<Slide id="what-is-react" bgColor="tertiary">
    <Heading size={1} fit textColor="black">
      What is React?
    </Heading>
  </Slide>),
  (<Slide transition={['fade']} bgColor="tertiary">
    <Heading size={1} fit textColor="black">
      What isn't React?
    </Heading>
  </Slide>),
  (<Slide bgColor="tertiary" notes={notes(`
    React is **just** the view layer. Unlike Angular or Ember or anything else
    to which it\'s often compared, React only handles your DOM; it says nothing
    about how to manage your data, contact the server, etc.
    `)}>
     <Heading size={2} caps textColor="primary">
       React is
     </Heading>
     <Heading size={1} caps textColor="black">
       not
     </Heading>
     <Heading size={2} caps textColor="primary">
       a framework
     </Heading>
  </Slide>),
  (<Slide bgColor="tertiary" notes={notes(`
    Even though you\'ll usually see React used with JSX (because JSX is so awesome),
    JSX is something separate from core React. You can use React without writing
    XML mixed into your JS code. It\'s just a little more verbose.
    `)}>
     <Heading size={2} caps textColor="primary">
       React is
     </Heading>
     <Heading size={1} caps textColor="black">
       not
     </Heading>
     <Heading size={2} caps textColor="primary">
       JSX
     </Heading>
     <CodePane lang="jsx" source={require('raw!../assets/todo.jsx.sample')} textSize="0.7em"/>
  </Slide>),
  (<Slide bgColor="tertiary" notes={notes(`
    Finally, React won\'t solve all your problems. It\'s just a view layer. Your
    code is still only as good as you can make it.

    So, given everything it\'s not, what are some of its benefits?
    `)}>
     <Heading size={2} caps textColor="primary">
       React is
     </Heading>
     <Heading size={1} caps textColor="black">
       not
     </Heading>
     <Heading size={2} caps textColor="primary">
       a cure-all
     </Heading>
  </Slide>),
  (<Slide id="app-declarative" bgColor="tertiary" notes={notes(`
    By far, the biggest benefit, even better than being able to write markup in your JavaScript files
    is that your application simply becomes a transformation of your data into visual elements. To
    highlight why this is such a big deal, consider a typical Backbone app. You\'d use some templating
    library to generate content for the page, then use jQuery calls or something similar to modify
    it when new data is available. This can quickly lead to an enormous mess, because...
    `)}>
     <Heading size={1} fit caps textColor="primary">
      Your app is a
     </Heading>
     <Heading size={1} fit caps textColor="black">
      declarative view
     </Heading>
     <Heading size={1} fit caps textColor="primary">
      of your data.
     </Heading>
  </Slide>),
  (<Slide id="state-is-bad" notes={notes(`
    ...(read slide)


    Whenever some part of your app is holding onto a value, another part of the app will expect it
    to have a different value. In order to reason about your application, you need to become a
    JavaScript interpreter, reading through the code and keeping track of which variables have
    which values. Tracing back through an app\'s code is a classic sign that you\'re dealing with...
    `)}>
     <Heading size={1} fit caps textColor="white">
      Application state
     </Heading>
     <Heading size={1} fit caps textColor="black">
      is the primary source
     </Heading>
     <Heading size={1} fit caps textColor="white">
      of software bugs
     </Heading>
  </Slide>),
  <Slide bgImage={require('../assets/spaghetti.jpg')} notes={notes(`
    ...spaghetti code.

    Now that we\'ve determined our app should just be a transformation of data into DOM elements,
    let\'s take a look at what that transform should look like.
    `)}/>,
  (<Slide bgColor="tertiary" id="todo-item-jsx" notes={notes(`
    `)}>
    <CodePane lang="jsx" textSize="0.8em" source={require('raw!../assets/todo-item.jsx.sample')} />
  </Slide>),
  (<Slide bgColor="white" id="todo-demo" notes={notes(`
    `)}>
    <Harness code={`<TodoItem complete={false} label="..."/>`} expose={{ TodoItem }} />
  </Slide>),
];
